FROM chrno-base

# set environment variables
ENV APP_USER="app" \
    APP_HOME="/usr/src/app"

ENV METEOR_PORT="3000"

# add nginx user and create his home directory
RUN useradd -d /home/$APP_USER -s /bin/bash $APP_USER
RUN mkdir /home/app; \
    chown -Rh app /home/app

# get nginx
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install nginx-light cron

# add nginx config
ADD nginx.conf /etc/nginx/nginx.conf

# expose ports
EXPOSE 80 443

# add nginx and cron services
ADD services/nginx /etc/service/nginx
ADD services/cron /etc/service/cron

# pull project
RUN git clone https://LimPull:lim-pull@bitbucket.org/lm_/mta-payments.git /usr/src/app

# chown project folder to app user
RUN chown -Rh app /usr/src/app

# download LetsEncrypt client
RUN curl -o /usr/local/bin/certbot-auto https://dl.eff.org/certbot-auto

# add crontab task
ADD renew.txt /tmp/renew.txt
RUN crontab /tmp/renew.txt

# goto project home dir
WORKDIR $APP_HOME

